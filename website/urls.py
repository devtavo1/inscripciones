from django.urls import path

from .views import *

from django.views.generic import TemplateView

app_name = 'website'

urlpatterns = [
    path('', index_website, name='index'),
    path('detalle-curso/<int:pk>/', detalle_curso, name='detalle_curso'),
    path('registro/', registro_alumno, name='registro_alumno'),
    path('pre-inscripcion/<int:pk>/', inscripcion_alumno, name='inscripcion_curso'),
    path('quienes-somos/', TemplateView.as_view(template_name='website/quienes_somos.html'), name='quienes_somos'),
    path('contacto/', contacto_website, name='contacto'),
    # path('inscripcion', TemplateView.as_view(template_name='website/inscripcion_curso.html'), name='inscripcion_curso'),
]
