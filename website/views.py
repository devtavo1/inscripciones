from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.core.mail import send_mail

from cursos.models import CursoOfertado, EstatusPreinscripcionCurso, PreinscripcionCurso
from website.forms import InscripcionAlumnoCursoForm, PreinscripcionAlumnoCursoModelForm, BusquedaCursoForm
from django.contrib import messages


def index_website(request):
    form = BusquedaCursoForm()

    curso_ofertado_lista = CursoOfertado.objects.filter(estatus_id=1)
    search = request.GET.get('search', None)
    nombre_curso = request.GET.get('nombre_curso', None)
    categoria_id = request.GET.get('categorias', None)

    if search:
        curso_ofertado_lista = curso_ofertado_lista.filter(curso__titulo__icontains=search)

    elif nombre_curso or categoria_id:

        if nombre_curso:
            curso_ofertado_lista = curso_ofertado_lista.filter(curso__titulo__icontains=nombre_curso)

        if categoria_id:
            curso_ofertado_lista = curso_ofertado_lista.filter(curso__categoria_id=categoria_id)


    context = {
        'curso_ofertado_lista': curso_ofertado_lista,
        'form': form
    }

    return render(request, 'website/index.html', context)


def detalle_curso(request, pk):
    inscrito = False
    curso_ofertado = CursoOfertado.objects.get(id=pk)
    if not request.user.is_anonymous:
        if PreinscripcionCurso.objects.filter(curso=curso_ofertado, prospecto=request.user).exists():
            inscrito = True
    # print(PreinscripcionCurso.objects.get(curso=curso_ofertado, prospecto=request.user))
    context = {'curso_ofertado': curso_ofertado, 'inscrito': inscrito}
    return render(request, 'website/detalle_curso.html', context)


def registro_alumno(request):
    form = UserCreationForm()

    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')

    context = {'form': form}
    return render(request, 'registration/registro_alumno.html', context)


@login_required
def inscripcion_alumno(request, pk):
    curso_ofertado = CursoOfertado.objects.get(pk=pk)
    form = PreinscripcionAlumnoCursoModelForm()
    if request.method == 'POST':
        form = PreinscripcionAlumnoCursoModelForm(request.POST, request.FILES)
        if form.is_valid():
            curso_ofertado_temp = form.save(commit=False)
            curso_ofertado_temp.prospecto = request.user
            curso_ofertado_temp.curso = curso_ofertado
            curso_ofertado_temp.estatus_id = 1
            curso_ofertado_temp.save()
            messages.success(request, 'Pre inscrito correctamente')
            return redirect('usuarios:perfil_alumno_cursos')

    context = {
        'form': form,
        'curso_ofertado': curso_ofertado
    }

    return render(request, 'website/inscripcion_curso.html', context)


class InscripcionAlumno(TemplateView):
    template_name = 'website/inscripcion_curso.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form = InscripcionAlumnoCursoForm()
        context['form'] = form
        return context

def contacto_website(request):
    if request.method == 'POST':
        email = request.POST.get('email', None)
        mensaje = request.POST.get('mensaje',None)

        if email and mensaje:
            try:
                send_mail('contacto desde la web', mensaje, email, ['tavo.jmnz91@gmail.com'])
                messages.success(request, 'Correo enviado con exito')
                return redirect('website:contacto')
            except Exception as e:
                messages.error(request, 'Ocurrio un error al enviar el correo, intentelo de nuevo')

    return render(request,'website/contacto.html')