from django import forms

from cursos.models import PreinscripcionCurso, CategoriaCurso


class InscripcionAlumnoCursoForm(forms.Form):
    comprobante_domicilio = forms.FileField()
    curp = forms.FileField()
    identificacion_oficial = forms.FileField()
    comprobante_pago = forms.FileField()
    comprobante_estudios = forms.FileField()
    acta_nacimiento = forms.FileField()


class PreinscripcionAlumnoCursoModelForm(forms.ModelForm):
    class Meta:
        model = PreinscripcionCurso
        fields = [
            'comprobante_domicilio',
            'curp',
            'identifacion_oficial',
            'comprobante_pago',
            'comprobante_estudio',
            'acta_nacimiento',
        ]


class BusquedaCursoForm(forms.Form):
    nombre_curso = forms.CharField(required=False, widget=forms.TextInput(attrs={'name':'search',}))
    categorias = forms.ModelChoiceField(queryset=CategoriaCurso.objects.all(), required=False)