from django import forms

from cursos.models import PreinscripcionCurso


class PreinscripcionCursoModelForm(forms.ModelForm):
    class Meta:
        model = PreinscripcionCurso
        exclude = ['curso', 'prospecto', 'estatus']
