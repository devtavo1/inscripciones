from django.contrib.auth.models import User
from django.db import models


class Curso(models.Model):
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    titulo = models.CharField(max_length=250)
    descripcion = models.TextField(blank=True)
    categoria = models.ForeignKey('CategoriaCurso', on_delete=models.CASCADE)

    class Meta:
        db_table = 'curso'
        ordering = ['titulo']

    def __str__(self):
        return self.titulo


class CursoOfertado(models.Model):
    sedes = (
        (1, 'Plantel Centro'),
        (2, 'Plantel Tenosique'),
    )

    instructor = models.CharField(max_length=250)
    precio = models.DecimalField(max_digits=8, decimal_places=2)  # 999999.99
    fecha_inicio = models.DateField(blank=True, null=True)
    sede = models.PositiveSmallIntegerField(choices=sedes)
    curso = models.ForeignKey(Curso, on_delete=models.CASCADE)
    capacidad = models.PositiveSmallIntegerField(default=15)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    estatus = models.ForeignKey('EstatusCursoOfertado', on_delete=models.CASCADE)

    class Meta:
        db_table = 'curso_ofertado'

    def __str__(self):
        return f'{self.curso.titulo} - {self.estatus.nombre}'


class EstatusCursoOfertado(models.Model):
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    nombre = models.CharField(max_length=250)

    class Meta:
        db_table = 'estatus_curso_ofertado'

    def __str__(self):
        return self.nombre


class CategoriaCurso(models.Model):
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    nombre = models.CharField(max_length=250)

    class Meta:
        db_table = 'categoria_curso'

    def __str__(self):
        return self.nombre


class EstatusPreinscripcionCurso(models.Model):
    nombre = models.CharField(max_length=250)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'estatus_preinscripcion_curso'

    def __str__(self):
        return self.nombre


class PreinscripcionCurso(models.Model):
    curso = models.ForeignKey(CursoOfertado, on_delete=models.CASCADE)
    prospecto = models.ForeignKey(User, on_delete=models.CASCADE)
    comprobante_domicilio = models.FileField(upload_to='archivos_inscripcion')
    curp = models.FileField(upload_to='archivos_inscripcion')
    identifacion_oficial = models.FileField(upload_to='archivos_inscripcion')
    comprobante_pago = models.FileField(upload_to='archivos_inscripcion', blank=True, null=True)
    comprobante_estudio = models.FileField(upload_to='archivos_inscripcion')
    acta_nacimiento = models.FileField(upload_to='archivos_inscripcion')
    estatus = models.ForeignKey(EstatusPreinscripcionCurso, on_delete=models.CASCADE)
    nota = models.CharField(max_length=250, blank=True)
    fecha_solicitud = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'preinscripcion_curso'

    def __str__(self):
        return f'{self.prospecto.first_name} - {self.curso.curso.titulo}'
