from django.shortcuts import render, redirect

from cursos.models import PreinscripcionCurso
from cursos.forms import PreinscripcionCursoModelForm
from usuarios.forms import UserModelForm
from django.contrib import messages


def perfil_alumno(request):
    form = UserModelForm(instance=request.user)
    if request.method == 'POST':
        form = UserModelForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            messages.success(request, 'Perfil actualizado correctamente')
            return redirect('usuarios:perfil_alumno')

    context = {
        'form': form,
    }
    return render(request, 'usuarios/perfil_aulmno.html', context)


def curso_alumno(request):
    cursos = PreinscripcionCurso.objects.filter(prospecto=request.user)
    context = {
        'cursos': cursos
    }
    return render(request, 'usuarios/cursos_alumno.html', context)


def curso_detalle_alumno(request, pk):
    curso = PreinscripcionCurso.objects.get(pk=pk)
    form = PreinscripcionCursoModelForm(instance=curso)

    if request.method == 'POST':
        form = PreinscripcionCursoModelForm(request.POST, request.FILES, instance=curso)
        if form.is_valid():
            form.save()
            return redirect('usuarios:perfil_alumno_cursos')
        else:
            print(form.errors)

    context = {
        'curso': curso,
        'form': form
    }
    return render(request, 'usuarios/curso_detalle_alumno.html', context)
