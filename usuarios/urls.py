from django.urls import path

from .views import *

app_name = 'usuarios'

urlpatterns = [
    path('perfil/alumno/cursos', curso_alumno, name='perfil_alumno_cursos'),
    path('perfil/alumno/', perfil_alumno, name='perfil_alumno'),
    path('perfil/alumno/cursos/detalle/<int:pk>/', curso_detalle_alumno, name='curso_detalle_alumno'),

]
