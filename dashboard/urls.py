from django.urls import path
from django.views.generic import TemplateView

from .views import *

app_name = 'dashboard'

urlpatterns = [
    path('', index_dashboard,name='index'),
    path('lista-curso/', lista_curso, name='lista_curso'),
    path('preinscripciones/', preinscripciones, name='preinscripciones'),
    path('preinscripcion/<int:pk>', preinscripcion_editar, name='preinscripcion_editar'),
]
