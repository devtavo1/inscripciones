from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from cursos.models import PreinscripcionCurso, CursoOfertado
from dashboard.forms import PreinscripcionDashboardModelForm


@login_required
def index_dashboard(request):
    if not request.user.groups.filter(name='administracion').exists():
        return redirect('website:index')

    return render(request, 'dashboard/index.html')


@login_required
def lista_curso(request):
    cursos_ofertados = CursoOfertado.objects.all()
    context = {
        'cursos_ofertados': cursos_ofertados
    }

    return render(request, 'dashboard/cursos/lista_curso.html', context)


@login_required
def preinscripciones(request):
    preinscripciones_lista = PreinscripcionCurso.objects.all()

    context = {
        'preinscripciones_lista': preinscripciones_lista,
    }
    return render(request, 'dashboard/cursos/preinscripciones.html', context)


@login_required
def preinscripcion_editar(request, pk):
    preinscripcion = PreinscripcionCurso.objects.get(pk=pk)
    form = PreinscripcionDashboardModelForm(instance=preinscripcion)
    if request.method == 'POST':
        form = PreinscripcionDashboardModelForm(request.POST, instance=preinscripcion)
        if form.is_valid():
            form.save()
            return redirect('dashboard:preinscripciones')

    context = {
        'preinscripcion': preinscripcion,
        'form': form,
    }
    return render(request, 'dashboard/cursos/preinscripciones_editar.html', context)
