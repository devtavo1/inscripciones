from django import forms

from cursos.models import PreinscripcionCurso


class PreinscripcionDashboardModelForm(forms.ModelForm):
    class Meta:
        model = PreinscripcionCurso
        fields = ['estatus', 'nota']
